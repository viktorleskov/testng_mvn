package junittask;

import junittask.plateau.Plateau;
import org.apache.logging.log4j.LogManager;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

public class PlateauTest {
    private static Plateau plateau;
    private static org.apache.logging.log4j.Logger LOG;

    @BeforeClass
    static void initAll() {
        LOG = LogManager.getLogger(PlateauTest.class);
    }

    @BeforeMethod
    void init() {
        plateau = new Plateau();
        LOG.info("BeforeEach running.");
    }

    @Test
    void plateauWorksProperlyTest() {
        LOG.info("plateauWorksProperlyTest test running.");
        plateau.setArray(1, 1, 1, 1, 1, 2, 2, 3, 3, 4, 5, 723, 7, 2, 4);
        assertEquals("0 5", plateau.getSequence());
    }

    @Test
    void testMethod2() {
        LOG.info("Second test running.");
        plateau.setArray(2, 2, 2, 3, 3, 3, 1, 1, 1, 4, 4, 4, 5, 5, 5, 5);
        assertFalse(plateau.getSequence().isEmpty());
    }

    @AfterMethod
    void tearDown() {
        LOG.info("After each running.");
    }

    @AfterClass
    static void tearDownAll() {
        LOG.info("AfterAll test running.");
        plateau = null;
        LOG = null;
    }
}
