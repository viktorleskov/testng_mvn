package junittask.plateau;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Plateau {
    private static int[] defaultVector = {1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5};
    private int[] array;

    public Plateau(int... array) {
        this.array = array;
    }
    //
    public Plateau() {
        this.array = defaultVector;
    }
    //
    public void setArray(int... array) {
        this.array = array;
    }

    public String getSequence() {
        Map<Integer, Integer> indexLengthMap = new LinkedHashMap<>();
        AtomicInteger maxSequenceIndex = new AtomicInteger();
        AtomicInteger maxSequenceLength = new AtomicInteger();
        for (int k = 0; k < array.length - 1; ) {
            int sequenceLength = calcLength(k);
            indexLengthMap.put(k, sequenceLength);
            k += sequenceLength;
        }

        indexLengthMap.forEach((index, value) -> {
            maxSequenceIndex.set(value > maxSequenceLength.intValue() ? index : maxSequenceIndex.intValue());
            maxSequenceLength.set(value > maxSequenceLength.intValue() ? value : maxSequenceLength.intValue());
        });
        return "" + maxSequenceIndex + " " + maxSequenceLength;
    }

    private int calcLength(int index) {
        int length = 1;
        while ((index < array.length - 1) && (array[index] == array[1 + index++])) {
            length++;
        }
        return length;
    }

}
